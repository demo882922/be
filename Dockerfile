FROM node:14

WORKDIR /be
COPY package.json .
RUN npm install
RUN npm install bcrypt
COPY . .
CMD npm start
