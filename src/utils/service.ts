import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import * as nodemailer from 'nodemailer';
import * as randomize from 'randomatic';
import * as NodeCache from 'node-cache';

@Injectable()
export class UtilsService {
  private readonly transporter: any;
  private myCache: any;
  constructor() {
    this.myCache = new NodeCache();
    this.transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        type: 'OAuth2',
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
      },
    });
  }
  encode(payload: any) {
    return jwt.sign(payload, process.env.PRIVATE_KEY, { expiresIn: '1h' });
  }
  decode(token: string) {
    return jwt.verify(token, process.env.PRIVATE_KEY);
  }
  sendMail(email: string, formMail: any) {
    console.log(email, formMail);
    const mailOptions = {
      from: 'DEMO',
      to: email,
      subject: formMail.subject,
      text: formMail.text,
      auth: {
        user: process.env.EMAIL,
        refreshToken: process.env.REFRESH_TOKEN,
        accessToken: process.env.ACCESS_TOKEN,
        expires: process.env.EXPIRES,
      },
    };
    this.transporter.sendMail(mailOptions);
  }

  sendVerifyCode(email, id) {
    const code = randomize('0', 6);
    this.sendMail(email, {
      subject: 'Email xác thực',
      text: `Mã xác thực của bạn là: ${code}`,
    });
    this.myCache.set(email, { code, id }, Number(process.env.EXPIRES_CODE));
  }

  verifyCode(email, code) {
    const value = this.myCache.get(email);
    if (value == undefined || value.code != code) return null;
    return value.id;
  }
}
