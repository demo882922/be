export { ResponsedStatus } from './responsedStatus';
import { HttpException, HttpStatus } from '@nestjs/common';
export class ForbiddenException extends HttpException {
  constructor(message?: string) {
    super(
      { 
        status: 1, 
        message: `aaaaaaa${message ? `: ${message}` : ''}` 
      }, 
      HttpStatus.OK
      );
  }
}
