export const ResponsedStatus = {
  INVALID_INPUT: {
    code: 1,
    message: 'Invalid input',
  },
  DATA_NOTFOUND: {
    code: 2,
    message: 'Data not found',
  },
  USERNAME_EXISTED: {
    code: 3,
    message: 'Username is exist',
  },
};
