import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AccountController } from './controller';
import { AccountEntity } from './entity';
import { AccountService } from './service';
import { UtilsModule } from 'src/utils/module';

@Module({
  imports: [UtilsModule, TypeOrmModule.forFeature([AccountEntity])],
  exports: [TypeOrmModule, AccountService],
  controllers: [AccountController],
  providers: [AccountService],
})
export class AccountModule {}
