import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Account } from './interface/account';
import { AccountEntity } from './entity';
import { ResponsedStatus } from 'src/constants';
import { UtilsService } from 'src/utils/service';
import {
  UpdateAccountDto,
  CreateAccountDto,
  VerifyAccountDto,
  FilterAccountDto,
} from './dto';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(AccountEntity)
    private accountRepository: Repository<AccountEntity>,
    private utilsService: UtilsService,
  ) {}

  async findByCondition(filter?: any): Promise<Account[]> {
    return this.accountRepository.find({
      where: filter,
      select: ['id', 'fullname', 'username', 'minutes', 'password'],
      order: { minutes: 'DESC' },
    });
  }

  async create(account: CreateAccountDto): Promise<Account> {
    const record = Object.assign(new AccountEntity(), {
      ...account,
      isActive: true, // lười lấy lại access token gửi mail
    });
    const existAccount = await this.accountRepository.findOne({
      where: { username: account.username },
    });
    if (existAccount) {
      throw new HttpException(ResponsedStatus.USERNAME_EXISTED, HttpStatus.OK);
    }
    const createdResult = await this.accountRepository.save(record);

    // this.utilsService.sendVerifyCode(createdResult.username, createdResult.id);
    delete createdResult.password;
    return createdResult;
  }

  async update(account): Promise<Account> {
    const record = Object.assign(new AccountEntity(), account);
    const updatedResult = await this.accountRepository.save(record);
    return updatedResult;
  }

  async increaseTime(id: number, minutes: number): Promise<Account> {
    const account = await this.accountRepository.findOne({ where: { id } });
    account.minutes += minutes;
    const updatedResult = await this.accountRepository.save(account);
    return updatedResult;
  }

  async sendVerifyCode(filterData: FilterAccountDto) {
    const account = await this.accountRepository.findOne({ where: filterData });

    if (!account)
      throw new HttpException(ResponsedStatus.DATA_NOTFOUND, HttpStatus.OK);

    this.utilsService.sendVerifyCode(account.username, account.id);
  }

  async verifyAccount(verifyData: VerifyAccountDto): Promise<Account> {
    const id = this.utilsService.verifyCode(
      verifyData.username,
      verifyData.code,
    );

    if (!id)
      throw new HttpException(ResponsedStatus.DATA_NOTFOUND, HttpStatus.OK);

    const record = Object.assign(new AccountEntity(), { id, isActive: true });
    const updatedResult = await this.accountRepository.save(record);
    return updatedResult;
  }
}
