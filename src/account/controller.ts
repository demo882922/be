import {
  Body,
  Query,
  Controller,
  Get,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { Account } from './interface/account';
import { AccountService } from './service';
import {
  CreateAccountDto,
  UpdateAccountDto,
  FilterAccountDto,
  VerifyAccountDto,
} from './dto';
import { CurrentUser, JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { UpdateLateTime } from './dto/update-account';

@Controller('account')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async findByCondition(
    @Query() filterAccountDto: FilterAccountDto,
  ): Promise<Account[]> {
    return this.accountService.findByCondition(filterAccountDto);
  }

  @Get('late-minutes')
  async getLateAcc(): Promise<Account[]> {
    const accounts = await this.accountService.findByCondition();
    return accounts.map((acc) => {
      const { password, ...rs } = acc;
      return rs as Account;
    });
  }

  @Post()
  async create(@Body() createAccountDto: CreateAccountDto): Promise<Account> {
    return this.accountService.create(createAccountDto);
  }

  @UseGuards(JwtAuthGuard)
  @Put()
  async update(@Body() updateAccountDto: UpdateAccountDto): Promise<Account> {
    console.log(updateAccountDto);
    return this.accountService.update(updateAccountDto);
  }

  @UseGuards(JwtAuthGuard)
  @Put('late-minutes')
  async updateTime(
    @CurrentUser() user: any,
    @Body() updatedData: UpdateLateTime,
  ): Promise<number> {
    const acc = await this.accountService.increaseTime(
      user.id,
      updatedData.minutes,
    );
    return acc.minutes;
  }

  @Post('/verify-account')
  async verifyAccount(@Body() verifyData: VerifyAccountDto): Promise<Account> {
    return this.accountService.verifyAccount(verifyData);
  }

  @Post('/send-verify-code')
  async sendVerifyCode(@Body() filterData: FilterAccountDto) {
    return this.accountService.sendVerifyCode(filterData);
  }
}
