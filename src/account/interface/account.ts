export class Account {
  id: number;
  username: string;
  password: string;
  fullname: string;
  isActive: boolean;
  minutes: number;
}
