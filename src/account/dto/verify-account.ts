import { IsNotEmpty } from 'class-validator';

export class VerifyAccountDto {
  @IsNotEmpty()
  readonly username: string;

  @IsNotEmpty()
  readonly code: string;
}
