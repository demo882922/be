export class LoginAccountDto {
  readonly username: string;
  readonly password: string;
}
