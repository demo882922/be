import { IsNotEmpty } from 'class-validator';

export class UpdateAccountDto {
  @IsNotEmpty()
  readonly id: number;

  readonly password: string;
  readonly fullname: string;
}

export class UpdateLateTime {
  @IsNotEmpty()
  readonly minutes: number;
}
