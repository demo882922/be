import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm';
import * as bcrypt from 'bcrypt';

@Entity('account')
export class AccountEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true, nullable: false })
  username: string;

  @Column({ nullable: false })
  password: string;

  @Column()
  fullname: string;

  @Column()
  isActive: boolean;

  @Column('int', { default: 0 })
  minutes: number;

  @BeforeInsert()
  @BeforeUpdate()
  hashPassword() {
    if (this.password) {
      const salt = bcrypt.genSaltSync(Number(process.env.SALT));
      this.password = bcrypt.hashSync(this.password, salt);
    }
  }
}
