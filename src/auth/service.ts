import { Injectable } from '@nestjs/common';
import { AccountService } from '../account/service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private readonly accountService: AccountService,
    private readonly jwtService: JwtService,
  ) {}

  async validateAccount(username: string, password: string): Promise<any> {
    const accounts = await this.accountService.findByCondition({ username });
    if (!accounts || !accounts.length) return null;

    const account = accounts[0];

    if (!bcrypt.compareSync(password, account.password)) return null;

    delete account.password;
    return account;
  }

  async login(account: any) {
    const payload = { username: account.username, id: account.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
